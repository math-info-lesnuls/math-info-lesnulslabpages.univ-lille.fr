# [<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-500.svg" height="50" style="vertical-align: middle">](https://gitlab.univ-lille.fr/math-info-lesnuls/math-info-lesnuls.gitlabpages.univ-lille.fr) [math-info-lesnuls](https://math-info-lesnuls.gitlabpages.univ-lille.fr/)

Ce dépôt contient les sources du site web [math-info-lesnuls](https://math-info-lesnuls.gitlabpages.univ-lille.fr/).

Il s'agit d'un site web statique généré avec [Hugo](https://gohugo.io/). Il a été crée lors de [la troisième séance](https://math-info-lesnuls.gitlabpages.univ-lille.fr/posts/git3/) sur le thème « Git ».