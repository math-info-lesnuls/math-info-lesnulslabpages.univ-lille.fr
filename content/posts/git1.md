---
title: "Git 1 : introduction"
date: 2024-01-16
---

## À quoi ça sert ?

Un projet (latex, python, etc.) est composé de plusieurs fichiers dans un répertoire et ses sous-répertoires qui évoluent au cours du temps. Git permet :

- à gérer les versions d'un projet (historique des modifications) ;
- à partager un projet sur internet (github, gitlab, etc.) ;
- à travailler à plusieurs sur un même projet ;
- à travailler sur plusieurs ordinateurs (maison, bureau, portable, etc.) ;
- à créer une page web (github ou gitlab pages).

## Installation

- Linux : en général, git est déjà installé, sinon `sudo apt install git`,
- [Mac](https://git-scm.com/download/mac) ou `brew install git` (si [brew](https://brew.sh/) est installé),
- [Windows](https://git-scm.com/download/win) : vient avec `git-bash` (un terminal `bash` pour Windows).

## Configuration

- `git config --global user.name "Prénom Nom"`
- `git config --global user.email "prenom.nom@univ-lille.fr"`
- `git config --global core.editor "code -w"` (ici `code` est l'éditeur [Visual Studio Code](https://code.visualstudio.com/), mais on peut mettre `vim`, `emacs`, etc.)

## Terminologie

- **dépôt** (_repository_) : un répertoire de travail avec un sous-répertoire `.git` qui contient l'historique des modifications ;
- **répertoire de travail** (_working (tree|directory)_) : répertoire de travail, c'est-à-dire le répertoire où l'on travaille (sans le répertoire `.git`) ;
- **index** (_cache_, _staging (area|index|tree)_) : liste des fichiers qui seront pris en compte lors du prochain commit ;
- **révision** (_commit_) : une modification (ajout, suppression, modification) d'un ou plusieurs fichiers dans l'historique du dépôt ; un commit contient :
  - un ou plusieurs fichiers (avec leur contenu),
  - un hash (une suite de caractères alphanumériques) qui identifie le commit,
  - un auteur (nom et email) et une date,
  - un message,
  - un ou plusieurs parents (les commits précédents).
- **branche** : pointeur vers un commit dans l'historique qui se déplace à chaque nouveau commit (la branche `master` ou `main` est la branche principale)
- **tag** : pointeur vers un commit dans l'historique qui ne se déplace pas ; permet de faire référence à un commit particulier au lieu d'utiliser son hash ;

## Créer un dépôt

- `git init` dans le répertoire du projet
- `git clone` pour récupérer un dépôt distant (sera vu dans [git 2](git%202.md))

## Cycle de vie d'un fichier

- `git add <fichier>` pour ajouter un fichier à l'index
- `git add .` pour ajouter tous les fichiers (non ignorés) du répertoire courant à l'index
- `git add -u` pour ajouter que les fichiers suivis (modifiés ou supprimés) à l'index
- `git commit` pour sauvegarder l'index dans l'historique
- `git commit -m "message"` pour sauvegarder l'index dans l'historique avec un message
- `git commit -am "message"` identique à `git add -u` puis `git commit -m "message"`

