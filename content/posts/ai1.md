---
title: "Les IA au quotidien 1"
date: 2025-02-25
---

## Les outils

### Les chats

- [ChatGPT](https://chat.openai.com)
- [Mistral](https://chat.mistral.ai)
- [Gemini](https://gemini.google.com)
- [Claude](https://claude.ai)

Fonctionnalités :
- joindre des fichiers
- choix du modèle
- canvas

## La personnalisation des IA

### Les assistants personnalisés

### Les prompts

- [un exemple](https://docs.google.com/document/d/1BykLzkWHRzZV3P49fDsDcrHzhQzZLRsdtwdPNXe7z3M/edit?tab=t.0) pour faire ChatGPT agir comme Perplexity.
- [un exemple]({{< relref "/extras/prompt_math1.md" >}}) pour résoudre des exercices de mathématiques, et une version plus « améliorée » [ici]({{< relref "/extras/prompt_math2.md" >}}).

Astuces pour un bon prompt :
- Structurer ses prompts en plusieurs parties
    - Donner des précisions sur le rôle de l'IA
    - Donner des précisions sur le contexte
    - Donner des précisions sur le résultat attendu
- Donner des exemples
- Demandez-lui de vous demander des précisions s'il en a besoin
- Indiquer la taille de réponse

Aide à la génération de prompts :
- Demander des exemples (à partir de résultats)
- Utiliser « Prompt Engineer » ou un autre assistant pour générer des prompts

## Des exemples d'utilisation 

- convertir un pdf en code LaTeX
- convertir une image en code tikz 
- raisoudre des exercices de mathématiques
- l'utiliser comme professeur particulier
- traduire des textes mathématiques
- générer des textes pour des mails

## Les bénéfices d'être universitaire

- Prix réduit pour « Le Chat Pro »

## Comparateur de modèles

https://www.comparia.beta.gouv.fr

---

[[2ᵉ exposé]({{< ref "ai2.md" >}})]