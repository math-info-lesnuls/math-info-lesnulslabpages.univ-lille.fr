---
title: "Git 3 : Création d'un site web"
date: 2024-02-12
---

## Objectifs

Créer un site web statique avec Git et [Gitlab Pages de l'université de Lille](https://gitlab.univ-lille.fr/).

## Note (un an plus tard) 

En décembre 2024, l'université de Lille a changé les adresses des pages web de Gitlab pour qu'ils soit accessible avec `https`. Toute les adresses `<quelquechose>.gitlabpages.univ-lille.fr/<chemin>` sont maintenant accessibles avec `https://gitlabpages.univ-lille.fr/<quelquechose>/<chemin>`. Ainsi : 
- le dépôt `https://gitlab.univ-lille.fr/prenom.nom/projet` est accessible à l'adresse `https://gitlabpages.univ-lille.fr/prenom.nom/projet/`;
- le dépôt `https://gitlab.univ-lille.fr/groupe/projet` est accessible à l'adresse `https://gitlabpages.univ-lille.fr/groupe/projet/`.
- le dépôt `https://gitlab.univ-lille.fr/prenom.nom/prenom.nom.gitlabpages.univ-lille.fr` est accessible à l'adresse `https://gitlabpages.univ-lille.fr/prenom.nom` (pas très logique, mais utilisable avec `https`);
- le dépôt `https://gitlab.univ-lille.fr/groupe/groupe.gitlabpages.univ-lille.fr` est accessible à l'adresse `https://gitlabpages.univ-lille.fr/groupe`.

## Création d'un site web en éditant des pages html

### Création d'un dépôt

Normalement votre login est de la forme `prenom.nom` et un dépôt nomé `projet` permet de créer un site web accessible à l'adresse `prenom.nom.gitlabpages.univ-lille.fr/projet/`. Sauf si le projet est nommé `prenom.nom.gitlabpages.univ-lille.fr`, comme le domaine _(la partie avant le premier `/`)_, alors le site sera accessible à la racine, c.-à-d. à l'adresse `prenom.nom.gitlabpages.univ-lille.fr/`.

Le grand problème avec nos login est que le caractère `.` empêche l'utilisation de `https` pour accéder au dépôt ! Donc en gros c'est inutilisable. La solution pour cela est de créer un groupe (dont le nom n'a pas de caractère `.`) et d'y ajouter le dépôt. Le site sera alors accessible à l'adresse `https://groupe.gitlabpages.univ-lille.fr/projet/` ou directement à l'adresse `https://groupe.gitlabpages.univ-lille.fr/` si le projet est nommé `groupe.gitlabpages.univ-lille.fr`.

Exemple :

On va créer un site web nommé `apropos` dans le groupe `math-info-lesnuls` qui va contenir une seule page « À propos » accessible à l'adresse [math-info-lesnuls.gitlabpages.univ-lille.fr/apropos](https://math-info-lesnuls.gitlabpages.univ-lille.fr/apropos).

1. Créer un groupe nommé `math-info-lesnuls` (sans caractère `.`) sur [https://gitlab.univ-lille.fr](https://gitlab.univ-lille.fr).
1. Créer un dépôt nommé `apropos` dans le groupe `math-info-lesnuls` : bouton `New project` > `Create blank project` > `Project name` : `apropos`, `Visibility Level` : `Public`, `Initialize repository with a README` : `No` > `Create project`.
1. Une page avec des instructions s'affiche. On suit les instructions :
    - **Git global setup** : si on ne l'a pas encore fait, c'est le moment.
    - **Create a new repository** : pour créer un dépôt sur votre ordinateur qui sera synchronisé avec ce dépôt. Exécuter les instructions dans un terminal, dans le dossier où vous voulez mettre le dossier `apropos`.
2. Créer un fichier `index.html` dans le dossier `public` avec le contenu suivant :

    ```html
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Info pour les nuls</title>
    </head>
    <body>
        <h1>Info pour les nuls</h1>
        <p>Bienvenue sur « Info pour les nuls ».</p>
    </body>
    </html>
    ```

3. Ajouter un fichier `.gitlab-ci.yml` à la racine du dépôt, avec le contenu suivant :

    ```yaml
    pages :
        script :
            - echo "Publication du site web"
        artifacts :
            paths :
                - public
    ```

1. Faire `git add`, `git commit` et `git push`. Lepage web est accessible à l'adresse [math-info-lesnuls.gitlabpages.univ-lille.fr/apropos](https://math-info-lesnuls.gitlabpages.univ-lille.fr/apropos).

    Si l'adresse affiché contient des chiffres et lettres barbares, il faut aller dans [Deploy > Pages](https://gitlab.univ-lille.fr/math-info-lesnuls/apropos/pages) et décocher `Use unique domain`, puis `Save changes`.

Remarque : On peut faire tout ça sur le wab, sans utiliser Git.

### Cas plus général

Le fichier `.gitlab-ci.yml` permet de définir des étapes de construction du site web. Dans la section `script` on peut mettre des commandes pour construire le site web dans le dossier `public`.

Par exemple, on peut utiliser le script suivant pour copier tous les fichiers (sauf ce qui commence par `.`) dans le dossier `public`, puis convertir les fichiers markdown en html (avec `pandoc`), et à la fin renommer les README.md en index.html :

```yaml
pages :
    image : alpine
    script :
        # installer pandoc
        - apk add pandoc
        # déplacer les fichiers (visibles) dans le dossier 'public'
        - mkdir .public
        - mv * .public
        - mv .public public
        # convertir les fichiers markdown en html
        - find public -name "*.md" -exec sh -c 'pandoc --from gfm --to html --standalone --metadata title="titre" "${0}" -o "${0%.md}.html"' {} \;
        # renommer les README.html en index.html
        - find public -name "README.html" -exec sh -c 'mv "${0}" "${0%README.html}index.html"' {} \;
    artifacts :
        paths :
            - public
```

## Création d'un site web en utilisant un générateur de site web statique

Nous allons utiliser le générateur de site web statique [Hugo](https://gohugo.io/) pour créer un blog. Hugo est un générateur de site web statique qui permet de créer des sites web en utilisant des fichiers markdown.

1. Créer un dépôt nommé `math-info-lesnuls.gitlabpages.univ-lille.fr` dans le groupe `math-info-lesnuls` : bouton `New project` > `Create blank project` > `Project name` : `math-info-lesnuls.gitlabpages.univ-lille.fr`, `Visibility Level` : `Public` > `Create project`.
1. Suivre les instructions, comme dans le premier exemple, pour répliquer le dépôt sur votre ordinateur (par exemple dans un dossier `math-info-lesnuls`) :
    ```bash
    > git clone git@gitlab-ssh.univ-lille.fr:math-info-lesnuls/math-info-lesnuls.gitlabpages.univ-lille.fr.git math-info-lesnuls
    > cd math-info-lesnuls
    > git switch --create main
    > git push --set-upstream origin main
    ```
1. Télécharger et installer [Hugo](https://gohugo.io/).
1. Créer un nouveau site web avec Hugo dans le dossier du dépôt de `math-info-lesnuls.gitlabpages.univ-lille.fr` :
    ```bash
    > hugo new site .
    ```
    Si le dossier n'est pas vide (contient le `README.md` par exemple) il faut rajouter `--force` :
    ```bash
    > hugo new site --force .
    ```
1. Choisir un thème pour le site web à l'adresse [https://themes.gohugo.io/](https://themes.gohugo.io/). Par exemple pour le thème [PaperMod](https://themes.gohugo.io/hugo-papermod/), décompresser [l'archive .zip](https://codeload.github.com/adityatelange/hugo-PaperMod/zip/refs/heads/master) dans le répertoire `themes/PaperMode` du site web.
1. Éditer le fichier `hugo.toml` pour ajouter le thème, changer le titre, changer l'adresse du site web, rajouter des menus. Ou le remplacer par `hugo.yaml` :

    ```yaml
    baseURL : https://math-info-lesnuls.gitlabpages.univ-lille.fr/
    languageCode : fr-fr
    title : Info pour les nuls
    theme : PaperMod
    menu :
        main :
            - name : Exposés
              url : /posts/
            - name : À propos
              url : /apropos/
    ```
1. Créer votre premier post (exécuter la commande dans le dossier du site web) :

    ```bash
    > hugo new posts/mon-premier-post.md
    ```
1. Éditer le fichier `content/posts/mon-premier-post.md` pour ajouter le contenu suivant :

    ```markdown
    ---
    title : "Mon premier post"
    date : 2024-02-13
    ---
    Bienvenue sur mon premier post.
    ```
1. Lancer le serveur web de développement :

    ```bash
    hugo server
    ```
1. Ouvrir un navigateur web à l'adresse [http://localhost:1313/](http://localhost:1313/).
1. Rajouter d'autre postes éventuellement.
1. Arrêter le serveur web de développement avec `Ctrl+C`.
1. Créer un `.gitlab-ci.yml` à la racine du dépôt avec le contenu suivant :

    ```yaml
    image : alpine

    pages :
        script :
            - apk add --no-cache --repository=https://dl-cdn.alpinelinux.org/alpine/edge/community hugo
            - hugo
        artifacts :
            paths :
                - public
    ```
1. Créer un fichier `.gitignore` :
    ```gitignore
    .gitignore
    .hugo_build.lock
    public
    ```
1.  Ajouter le site web au dépôt Git :

    ```bash
    git add .
    git commit -m "Première version du blog"
    git push
    ```
1. Vérifier dans `Deploy > Pages` que `Use unique domain` est décoché.

    Le site web est accessible à l'adresse [math-info-lesnuls.gitlabpages.univ-lille.fr](https://math-info-lesnuls.gitlabpages.univ-lille.fr).
