---
title: "Les IA au quotidien 2"
date: 2025-03-04
---

## Les outils

### Les moteurs de recherche

- [Perplexity](https://perplexity.ai)
- [EleutherAI](https://eleuther.ai)
- des « assistants personnalisés » des chats génériques

### Exploration de documents

- Google [NotebookLM](https://notebooklm.google/)

### Assistant de programmation (rédaction)

- [Github Copilot](https://copilot.github.com) : gratuit pour les étudiants et les enseignants
- [Gemini Code Assist](https://codeassist.google/) : gratuit et compatible avec VSCodium
- [Continue](https://www.continue.dev/) : open source, agents locaux et cloud

## Des exemples d'utilisation 

- générer le plan d'un cours
- synthétiser des textes ou des documents (générer des résumés)
- générer des formules pour Excel
- générer code python pour faire des expériences
- générer des images

## Les bénéfices d'être universitaire

- [Student Developer Pack](https://education.github.com) pour Copilot. Les instructions sont [ici](https://docs.github.com/fr/education/explore-the-benefits-of-teaching-and-learning-with-github-education/github-education-for-teachers/apply-to-github-education-as-a-teacher).

---

[[1ᵉʳ exposé]({{< ref "ai1.md" >}})]
