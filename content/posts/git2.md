---
title: "Git 2 : historique et serveurs distants"
date: 2024-01-23
---

## Historique

### Les états possibles d'un fichier

- `git status` pour voir l'état des fichiers ; les fichiers peuvent être :
  - **ignorés** (_ignored_) : fichiers qui sont dans le fichier `.gitignore` et qui sont « invisibles » pour git (voir plus bas),
  - **non suivis** (_untracked_) : fichiers qui n'ont jamais été ajoutés à l'index (ceci est un état temporaire, ils deviendront suivis ou ignorés normalement),
  - **suivis** (_tracked_) : fichiers qui ont été par le passé ajoutés à l'index, est qui sont maintenant dans l'un des états suivants :
    - **non modifiés** (_unmodified_) : qui sont identiques à la dernière version du dépôt et dans l'index,
    - **modifiés** (_modified_) : fichiers qui ont été modifiés depuis le dernier ajout à l'index,
    - **indexés** (_staged_) : qui sont identiques à la dernière version de l'index, mais qui ont été modifiés depuis le dernier commit.
- `git log` pour voir l'historique des commits
- `git log --oneline --graph` pour voir l'historique des commits sur une ligne avec les branches

### Le fichier .gitignore

Exemple de fichier `.gitignore` pour un projet LaTeX

```sh
# fichiers à ignorer
.gitignore
*.aux
*.log
*.out
*.toc
*.synctex.gz

# dossier à ignorer
examen
```

### Récupérer des fichiers

- `git restore <fichier>` pour récupérer un fichier à partir de l'index
- `git restore --staged <fichier>` pour récupérer un fichier à partir du dernier commit dans l'index
- `git restore --staged --worktree <fichier>` pour récupérer un fichier à partir du dernier commit dans le répertoire de travail et dans l'index
- `git restore --source <commit> <fichier>` pour récupérer un fichier à partir d'un commit donné dans le répertoire de travail et dans l'index

### Modifier l'historique

- `git commit --amend` pour remplacer le dernier commit au lieu d'en créer un nouveau
- `git reset --soft HEAD^` pour annuler le dernier commit et revenir à l'état précédent (sans modifier le répertoire de travail)

Il y a d'autres commandes (`revert`, `checkout`...) pour modifier l'historique, mais elles ne sont pas présentées ici.

## Serveur distant

### Commandes de base

- `git clone` pour récupérer un dépôt distant
- `git push` pour envoyer les commits locaux vers le dépôt distant
- `git pull` pour récupérer les commits distants vers le dépôt local

Il y a d'autres commandes (`fetch`, `merge`...) pour partager un dépôt, mais elles ne sont pas présentées ici. Lors de la création d'un dépôt sur un serveur distant (github, gitlab, etc.) il faut **suivre les instructions** du serveur pour créer le dépôt et le lier au dépôt local.

### Création d'un dépôt sur un serveur distant

On va considérer ici seulement le cas de gitlab de l'université de Lille. Pour créer un dépôt sur gitlab, il faut :
1. Se connecter à <https://gitlab.univ-lille.fr> avec son identifiant et son mot de passe de l'université.
2. Je vous conseille de créer un groupe pour votre projet (par exemple `maths-info-lesnuls` pour le projet « Info pour les nuls » ou tout simplement `nom-de-faille` si c'est que pour vous). Pour cela, il faut cliquer sur le bouton « + » à côté de votre photo, puis sur « New group » et **suivre les instructions**. Le nom de groupe ne doit pas contenir de point, car ça pose problème pour la création du site web. Vous pouvez créer plusieurs dépôts dans le même groupe.
3. Ensuite, il faut cliquer sur le bouton « New project » et **suivre les instructions**.

## Parties non présentées lors de la séance

### Branches

- `git branch` pour voir les branches
- `git branch nom-de-branche` pour créer une branche
- `git switch nom-de-branche` pour changer de branche
- `git merge nom-de-branche` pour fusionner une branche dans la branche courante
- `git branch -d nom-de-branche` pour supprimer une branche

### Quelques alias utiles (non présenté lors de la séance)

- `git config --global alias.st status` pour pouvoir taper `git st` au lieu de `git status`
- `git config --global alias.co commit` pour pouvoir taper `git co` au lieu de `git commit`
- `git config --global alias.lol "log --graph --decorate --pretty=oneline --abbrev-commit --all"` pour obtenir un historique plus lisible avec `git lol`
