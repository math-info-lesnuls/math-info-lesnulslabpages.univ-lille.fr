---
title: "Introduction à TikZ"
date: 2024-03-19
---

## La classe `standalone`

Avant de commencer à dessiner des figures avec TikZ, il est important de savoir que la classe `standalone` est très utile pour créer des images TikZ. Elle permet de créer des images sous forme de fichiers `pdf` qui peuvent être inclus dans un document LaTeX avec la commande `\includegraphics`.

Le code suivant permet de créer un pdf d'une seule page dont la taille est ajustée au contenu (ici le texte, mais cela fonctionne aussi avec une image TikZ).

```latex
\documentclass{standalone}
\begin{document}
    Ceci est une mage composée de texte.
\end{document}
```

## La bibliothèque `tikz`

La bibliothèque `tikz` est nécessaire pour utiliser TikZ. Elle est incluse dans le préambule du document avec la commande `\usepackage{tikz}`. Voici un exemple de code TikZ.

```latex
\documentclass{standalone}
\usepackage{tikz}
\begin{document}
    \begin{tikzpicture}
        \fill[red] (0,0) circle(1 cm);
        \draw[very thick,<->] (-1,0) -- (1,0);
        \path (0,0) node[above]{$d$};
    \end{tikzpicture}
\end{document}
```
[tikz](exemple-tikz1.tex) [pdf](exemple-tikz1.pdf)

### Explications
- Cette image est composée de 3 commandes : `\fill`, `\draw` et `\path` qui se terminent par un point-virgule.
- Les coordonnées sont données en unités par défaut de 1 cm. Donc cette image devrait être de taille 2 cm × 2 cm.
- La commande `\fill[red] (0,0) circle(1 cm);` permet de remplir un cercle de rayon 1 cm avec la couleur rouge.
- La commande `\draw[very thick,<->] (-1,0) -- (1,0);` permet de tracer une ligne très épaisse avec des flèches aux deux extrémités.
- La commande `\path (0,0) node[above]{$d$};` permet de placer « un noeud » au-dessus du point `(0,0)` avec le texte `$d$`.
- La commande `\path` est la commande de base et les deux autres commandes sont des raccourcis : - `\fill` est un raccourci pour `\path[fill]`, - - `\draw` est un raccourci pour `\path[draw]`.
- On peut combiner les deux actions avec `\path[draw,fill]`.
- Ce qui se trouve entre les crochets `[...]` sont des « styles ».

### Les coordonnées

- `(1,2)` c'est la même chose que `(1 cm,2 cm)`.
- `(35:2)` sont des coordonnées polaires. C'est un angle de 35 degrés et un rayon de 2 cm.
- `+(1,1)` est un déplacement relatif (par rapport à la position actuelle) et la position actuelle reste inchangée.
- `++(1,1)` est un déplacement relatif (par rapport à la position actuelle) et la position actuelle est modifiée.
- `coordinate(A)` enregistre la position actuelle dans un nœud nommé `A`, puis on peut utiliser `(A)` comme des coordonnées.
- `cycle` permet de « fermer » le chemin.

_Exemple :_ [tikz](exemple-tikz2.tex) [pdf](exemple-tikz2.pdf).

### Les opérations de base

- `(A) -- (B)` : ligne droite entre `(A)` et `(B)`;
- `(A) circle (1 cm)` : cercle de rayon 1 cm centré en `(A)`;
- `(A) ellipse (1 cm and 2 cm)` : ellipse de largeur 1 cm et de hauteur 2 cm centrée en `(A)`;
- `(A) arc(0:90:1 cm)` : arc de cercle de 0 à 90 degrés de rayon 1 cm centré en `(A)`;
- `(A) .. controls (C) and (D) .. (B)` : courbe de Bézier de `(A)` à `(B)` avec des points de contrôle `(C)` et `(D)`.
- `(A) rectangle (B)` : rectangle avec deux coins opposés en `(A)` et `(B)`;
- `(A) grid (B)` : grille avec des lignes horizontales et verticales passant par `(A)` et `(B)`;
- `(A) node {text}` : nœud avec le texte `text` centré en `(A)`;

### Les transformations

Les transformations suivantes s'appliquent aux coordonnées, mais pas aux épaisseurs des lignes, aux tailles des textes, etc. Elles ne s'appliquent pas non plus aux coordonnées nommées, comme `(A)`.

- `[xshift=1 cm]` : déplacement à droite de 1 cm;
- `[yshift=-1 mm]` : déplacement vers le bas de 1 mm;
- `[rotate=30]` : rotation de 30 degrés (de centre `(0,0)`);
- `[scale=2]` : agrandissement de 2;

Si on veut appliquer ces transformations à tout le dessin, on peut les mettre juste après `\begin{tikzpicture}`.

Et pour appliquer ces transformations à une partie du dessin, on peut utiliser l'environnement `scope`.

_Exemple :_ [tikz](exemple-tikz3.tex) [pdf](exemple-tikz3.pdf).


### Les graphes de fonctions

La commande `plot` permet de tracer des graphes de fonctions « simples ». Par exemple, la commande

```latex
    \draw[domain=0:2*pi] plot (\x,{sin(\x r)});
```
[tex](exemple-tikz4.tex) [pdf](exemple-tikz4.pdf)

permet de tracer le graphe de la fonction sinus sur l'intervalle `[0,2π]`.

Pour des fonctions plus compliquées, il est possible d'utiliser la bibliothèque `pgfplots` qui est une extension de TikZ.

```latex
\documentclass{standalone}
\usepackage{pgfplots}
\begin{document}
    \begin{tikzpicture}
        \begin{axis}[view={21}{42}]
            \addplot3[surf,domain=0:360] {cos(x)*cos(y)};
        \end{axis}
    \end{tikzpicture}
\end{document}
```
[tex](exemple-tikz5.tex) [pdf](exemple-tikz5.pdf)
